import config from 'config';
import { DataStoredInToken } from '@interfaces/user.interface';
import userModel from '@models/user.model';
import { User } from '@interfaces/user.interface';
import jwt from 'jsonwebtoken';

const authMiddleware = async (socket, next) => {
  try {
    const Authorization = socket.handshake.headers.authorization.split('Bearer ')[1] || null;

    if (Authorization) {
      const secretKey: string = config.get('secretKey');
      const verificationResponse = await jwt.verify(Authorization, secretKey) as DataStoredInToken;
      const userId = verificationResponse._id;
      const findUser: User = await userModel.findById(userId);

      if (findUser) {
        socket.user = findUser;
        next();
      } else {
        next(new Error('Wrong authentication token'));
      }
    } else {
      next(new Error('Authentication token missing'));
    }
  } catch (error) {
    next(new Error('Wrong authentication token'));
  }
};

export default authMiddleware;
