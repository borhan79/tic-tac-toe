import socketIo from "socket.io";
import { GameEvents } from "@enums/events.enum";
import { SocketWithUser } from "@interfaces/socket.interface";
import BoardService from "@services/board.service";
import RedisService from "@services/redis.service";
import { v4 } from "uuid";
import XoChecker from "@utils/xoChecker";
import ConfigService from "@services/config.service";
import UserService from "@services/user.service";

class SocketService {

  private boardService = new BoardService();
  private redisService = new RedisService();
  private configService = new ConfigService();
  private userService = new UserService();

  private chooseRandomTurn(): string {
    const symbols: Array<string> = ["X", "O"];
    return symbols[Math.floor(Math.random() * symbols.length)];
  }

  public async joinRoomEvent (io: socketIo.Server, socket: SocketWithUser, roomId: string){

      const userRoom = await this.redisService.getUserCurrentRoom(socket.user._id);
      if (userRoom !== null) {
        socket.emit(GameEvents.SERVER_MESSAGE, "You're currently in a game");
        return;
      }

      const roomCredit =  await this.boardService.getBoardCredit(roomId)
      if (roomCredit === null){
        socket.emit(GameEvents.SERVER_MESSAGE, `No room found with this id`);
        return;
      }
    const userCredit = await this.userService.getUserCredit(socket.user._id)

    if (userCredit < roomCredit){
        socket.emit(GameEvents.SERVER_MESSAGE, `You don't have enough credit to join this room. your current credit is: ${userCredit}`);
        return;
      }

      await socket.join(roomId);

      await this.redisService.saveRoomUsers(roomId, socket.user._id.toString())

      await this.boardService.updateBoardOpponent(roomId, socket.user._id);
      await this.boardService.updateBoardStatus(roomId);

      socket.emit(GameEvents.SERVER_MESSAGE, `Game is starting... . Your symbol is X`);
      socket.broadcast.to(roomId).emit(GameEvents.SERVER_MESSAGE, `${socket.user.NickName} has joined. Game is starting... .Your symbol is O`);

      await this.redisService.saveUserCurrentRoom(roomId, socket.user._id);
      await this.redisService.saveUserSymbol(socket.user._id, "X");

      const turnSymbol = this.chooseRandomTurn();
      if (turnSymbol === "X") {
        socket.emit(GameEvents.SERVER_MESSAGE, `It's your turn`);
      } else {
        socket.broadcast.to(roomId).emit(GameEvents.SERVER_MESSAGE, `It's your turn`);
      }
      await this.redisService.saveRoomTurn(roomId, turnSymbol);

      await this.redisService.saveRoomArray(roomId);
      await this.redisService.saveRoomResult(roomId, "onGoing");
      await this.redisService.saveRoomCredit(roomId, roomCredit)

  }

  public async playGroundEvent(io: socketIo.Server, socket: SocketWithUser, index: number){
    const userRoom = await this.redisService.getUserCurrentRoom(socket.user._id);
    if (userRoom === null) {
      socket.emit(GameEvents.SERVER_MESSAGE, "You're not in a active game currently");
      return;
    }
    const userSymbol = await this.redisService.getUserSymbol(socket.user._id);
    const turnSymbol = await this.redisService.getRoomTurn(userRoom);
    if (userSymbol !== turnSymbol) {
      socket.emit(GameEvents.SERVER_MESSAGE, "It's not your turn");
      socket.broadcast.to(userRoom).emit(GameEvents.SERVER_MESSAGE, `It's your turn`);
      return;
    }

    const xoArray = await this.redisService.getRoomArray(userRoom);
    if (xoArray[index] != 0) {
      socket.emit(GameEvents.SERVER_MESSAGE, "Chosen index is already selected or out of range, choose another index");
      return;
    }

    xoArray[index] = userSymbol;
    await this.redisService.updateRoomArray(userRoom, index, userSymbol);

    const checkEqual = await XoChecker.checkEqual(xoArray)
    if (checkEqual === true) {
      io.to(userRoom).emit(GameEvents.SERVER_MESSAGE, `Nobody Won ...`);
      await this.redisService.saveRoomResult(userRoom, 'Equal');
      await this.endGameEvent(io, socket)
      return
    }

    const checkWin = XoChecker.checkWinner(xoArray);
    if (checkWin === userSymbol) {
      io.to(userRoom).emit(GameEvents.SERVER_MESSAGE, `${socket.user.NickName} Won a game`);
      await this.redisService.saveRoomResult(userRoom, socket.user._id.toString());
      await this.endGameEvent(io, socket)
      return
    }

    await this.redisService.saveRoomTurn(userRoom, turnSymbol === "X" ? "O" : "X");
    socket.broadcast.to(userRoom).emit(GameEvents.SERVER_MESSAGE, `It's your turn`);
  }

  public async endGameEvent (io: socketIo.Server, socket: SocketWithUser){
    const userRoom = await this.redisService.getUserCurrentRoom(socket.user._id);
    let roomResult = await this.redisService.getRoomResult(userRoom);
    const roomCredit = await this.redisService.getRoomCredit(userRoom)
    const roomUsers: Array<string> = await this.redisService.getRoomUsers(userRoom)
    if (roomResult === "onGoing" || roomResult === 'waitingForOpponent') {
      roomResult = "Aborted";
    }else{
      roomUsers.map( async (user: string) => {
        if (user == socket.user._id){
          await this.userService.updateUserCredit(user, parseInt(roomCredit));
        }else{
          await this.userService.updateUserCredit(user, -parseInt(roomCredit));
        }
      })
    }
    await this.boardService.endGameUpdate(userRoom, roomResult);
    await this.redisService.delUserData(roomUsers[0]);
    await this.redisService.delUserData(roomUsers[1]);
    await this.redisService.delRoomData(userRoom);
    io.to(userRoom).emit(GameEvents.SERVER_MESSAGE, `Game Finished`);
  }

  public async lobbyEvent(io: socketIo.Server, socket: SocketWithUser){
    io.to(socket.id).emit(GameEvents.SERVER_MESSAGE, `You are in lobby now`);
    const notYetStartedBoards = await this.boardService.notYetStartedBoards();
    io.to(socket.id).emit(GameEvents.SERVER_MESSAGE, `Here is a list of not yet started boards which you can join them: ${notYetStartedBoards}`);
  }

  public async waitingEvent(io: socketIo.Server, socket: SocketWithUser, credit: number){
    const userRoom = await this.redisService.getUserCurrentRoom(socket.user._id);
    if (userRoom !== null) {
      socket.emit(GameEvents.SERVER_MESSAGE, "You're currently in a game");
      return;
    }

    const validCredits = await this.configService.getConfigValidCredits()
    if (!validCredits.includes(credit)){
      socket.emit(GameEvents.SERVER_MESSAGE, `Please enter valid credit. valid credits: ${validCredits}`);
      return;
    }
    const userCredit = await this.userService.getUserCredit(socket.user._id)
    if (userCredit < credit){
      socket.emit(GameEvents.SERVER_MESSAGE, `You don't have enough credit. your current credit is: ${userCredit}`);
      return;
    }
    const roomId = v4();
    const createBoard = await this.boardService.createBoard(socket.user._id, roomId, credit);
    socket.join(roomId);
    io.to(socket.id).emit(GameEvents.SERVER_MESSAGE, `Your game created successfully. Here is your BoardId to invite opponent: ${createBoard.BoardId}`);
    io.to(socket.id).emit(GameEvents.SERVER_MESSAGE, `Your symbol is O`);
    await this.redisService.saveUserCurrentRoom(roomId, socket.user._id);
    await this.redisService.saveUserSymbol(socket.user._id, "O");
    await this.redisService.saveRoomUsers(roomId, socket.user._id.toString())
    await this.redisService.saveRoomResult(roomId, "waitingForOpponent");

  }

  public async sendMessageEvent(io: socketIo.Server, socket: SocketWithUser, msg: string){
    const bannedWords: Array<string> = await this.configService.getConfigBannedWords()
    const splittedMessage: Array<string> = msg.split(' ')
    let checkBannedWords = splittedMessage.some( word => {
      return bannedWords.includes(word);
    })
    if (checkBannedWords === true){
      socket.emit(GameEvents.SERVER_MESSAGE, 'PLease be polite :)))))))')
      return;
    }
    const userRoom = await this.redisService.getUserCurrentRoom(socket.user._id);
    socket.broadcast.to(userRoom).emit(GameEvents.CHAT_MESSAGE, msg);
  }

}

export default SocketService;
