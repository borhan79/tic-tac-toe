import userModel from "@models/user.model";
import { UserDto } from "@dtos/user.dto";
import { DataStoredInToken, User } from "@interfaces/user.interface";
import HttpException from "@utils/httpException";
import config from "config";
import jwt from "jsonwebtoken";
import path from 'path';
import { promises as fs } from 'fs';

class UserService {
  private user = userModel;

  public async createUser(userData: UserDto): Promise<User> {
    const findUser: User = await this.user.findOne({ NickName: userData.NickName });
    if (findUser) throw new HttpException(409, `User ${findUser.NickName} already exists`);

    return await this.user.create(userData);
  }

  public async updateUser(userData: UserDto): Promise<User> {
    const findUser: User = await this.getUser(userData.NickName);
    if (userData.Avatar){
      const base64 = userData.Avatar.split(';base64,').pop()
      const root_path = path.join(__dirname, `../../uploads/${findUser.NickName}-${Date.now()}`)
      await fs.writeFile(root_path,
        base64, {encoding: 'base64'});
      userData.Avatar = root_path
    }
    return this.user.findOneAndUpdate({ NickName: userData.NickName }, userData);
  }

  public async deleteUser(NickName: string): Promise<User> {
    const findUser: User = await this.getUser(NickName);
    return this.user.findOneAndRemove({ NickName });
  }

  public async getUser(NickName: string): Promise<User> {
    const findUser: User = await this.user.findOne({ NickName });
    if (!findUser) throw new HttpException(409, `User ${NickName} not found`);
    return findUser;
  }

  public async createToken(user: User): Promise<string> {
    const dataStoredInToken: DataStoredInToken = { _id: user._id, NickName: user.NickName};
    const secret: string = config.get("secretKey");
    return await jwt.sign(dataStoredInToken, secret, { expiresIn: "5h" });
  }

  public async updateUserCredit(userId: string, credit: number): Promise<User> {
    return await this.user.findOneAndUpdate({ _id: userId }, { $inc: { Credit: credit } });
  }

  public async getUserCredit(userId: string): Promise<number>{
    const userData: User =  await this.user.findOne({ _id: userId });
    return userData.Credit
  }
}

export default UserService;
