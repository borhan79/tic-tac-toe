import boardModel from "@models/board.model";
import { Board } from "@interfaces/board.interface";

class BoardService {
  private board = boardModel

  public async createBoard(owner: string, id: string, credit: number): Promise<Board>{
    return await this.board.create({ Owner: owner, BoardId: id , Credit: credit});
  }

  public async notYetStartedBoards(): Promise<Board[]> {
    return await this.board.find({ Status: 0 });
  }

  public async updateBoardStatus(id: string): Promise<Board> {
    return this.board.findOneAndUpdate({BoardId: id}, {Status: 1})
  }

  public async updateBoardOpponent(boardId: string,userId: string): Promise<Board>{
    return await this.board.findOneAndUpdate({BoardId: boardId}, {Opponent: userId})
  }

  public async endGameUpdate(boardId: string, result: string){
    return await this.board.findOneAndUpdate({BoardId: boardId}, {Status: 2, Result: result})
  }

  public async getBoardCredit(boardId: string): Promise<number>{
    const boardData: Board = await this.board.findOne({ BoardId: boardId });
    return boardData.Credit
  }
}

export default BoardService;
