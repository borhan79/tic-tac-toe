import App from "@/app";

class RedisService {
  private redis = App.redisClient

  public async saveUserCurrentRoom(roomId: string, userId: string){
    return await this.redis.set(`user_room:${userId}`, roomId)
  }

  public async getUserCurrentRoom(userId: string):Promise<string> {
    return this.redis.get(`user_room:${userId}`)
  }

  public async saveRoomUsers(roomId: string, userId: string){
    return await this.redis.lpush(`room_users:${roomId.trim()}`, [userId])
  }

  public async getRoomUsers(roomId: string){
    return await this.redis.lrange(`room_users:${roomId.trim()}`,0,-1)
  }

  public async delUserData(userId: string){
    return await this.redis.del(`user_room:${userId}`, `user_symbol:${userId}`)
  }


  public async saveRoomTurn(roomId: string, symbol: string) {
    return await this.redis.set(`room_turn:${roomId.trim()}`, symbol)
  }

  public async getRoomTurn(roomId: string){
    return await this.redis.get(`room_turn:${roomId}`)
  }

  public async saveUserSymbol(userId: string, symbol: string){
    return await this.redis.set(`user_symbol:${userId}`, symbol)
  }

  public async getUserSymbol(userId: string){
    return await this.redis.get(`user_symbol:${userId}`)
  }

  public async saveRoomArray(roomId: string){
    return await this.redis.lpush(`room_array:${roomId.trim()}`, [0,0,0,0,0,0,0,0,0])
  }

  public async getRoomResult(roomId: string){
    return await this.redis.get(`room_result:${roomId}`)
  }

  public async saveRoomResult(roomId: string, result: string){
    return await this.redis.set(`room_result:${roomId.trim()}`, result)
  }

  public async saveRoomCredit(roomId: string, credit: number){
    return await this.redis.set(`room_credit:${roomId.trim()}`, credit)
  }

  public async getRoomCredit(roomId: string){
    return await this.redis.get(`room_credit:${roomId.trim()}`)
  }

  public async getRoomArray(roomId: string){
    return await this.redis.lrange(`room_array:${roomId}`, 0,-1)
  }

  public async updateRoomArray(roomId: string, index: number, userSymbol: string){
    return await this.redis.lset(`room_array:${roomId}`, index, userSymbol)
  }

  public async delRoomData(roomId: string){
    return await this.redis.del(`room_array:${roomId}`, `room_result:${roomId}`, `room_turn:${roomId}`, `room_users:${roomId}`, `room_credit:${roomId}`)
  }

}

export default RedisService;
