import configModel from "@models/config.model";
import { CreateConfigDto, UpdateConfigDto } from "@dtos/config.dto";
import { Config } from "@interfaces/config.interface";
import HttpException from "@utils/httpException";

class ConfigService {
  private config = configModel

  public async createConfig(configData: CreateConfigDto): Promise<Config>{
    const configDocument: Config = await this.config.findOne({configId: 1})
    if (configDocument){
      throw new HttpException(400, 'You cant create more than one config document')
    }
    return await this.config.create(configData)
  }

  public async updateConfig(configData: UpdateConfigDto, status: number): Promise<Config>{
    if (status == 1)
      return this.config.findOneAndUpdate({ configId: 1 }, {
        $set: configData
      });
    else if (status == 2)
      return this.config.findOneAndUpdate({ configId: 1 }, {
        $push: configData
      });
    else
      return this.config.findOneAndUpdate({ configId: 1 }, {
        $pull: configData
      });
  }
  
  public async getConfigValidCredits(): Promise<Array<number>> {
    const configData: Config = await this.config.findOne({ configId: 1 })
    return configData.validCredits
  }

  public async getConfigBannedWords(): Promise<Array<string>> {
    const configData: Config = await this.config.findOne({ configId: 1 })
    return configData.bannedWords
  }

}

export default ConfigService;
