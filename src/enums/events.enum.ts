export enum GameEvents {
  CONNECT = 'connection',
  DISCONNECT = 'disconnect',
  CHAT_MESSAGE = 'chatMessage',
  SERVER_MESSAGE = 'serverMessage',
  JOIN_ROOM = 'joinRoom',
  END_GAME = 'endGame',
  LOBBY = 'lobby',
  WAITING = 'waiting',
  PLAYGROUND = 'playGround',
  SEND_MESSAGE = 'sendMessage'
}
