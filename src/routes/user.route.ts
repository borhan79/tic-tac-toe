import { Router } from 'express';
import UserController from '@controllers/user.controller';
import { DeleteUserDto, UserDto } from "@dtos/user.dto";
import Route from '@interfaces/route.interface';
import validationMiddleware from '@middlewares/validation.middleware';

class UserRoute implements Route {
  public path = '/api/v1/admin/';
  public router = Router();
  public userController = new UserController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}user`, validationMiddleware(UserDto, 'body',true), this.userController.createUser);
    this.router.put(`${this.path}user`, validationMiddleware(UserDto, 'body',true), this.userController.updateUser);
    this.router.delete(`${this.path}user`, validationMiddleware(DeleteUserDto, 'query'), this.userController.deleteUser);
    this.router.post(`${this.path}token`, validationMiddleware(UserDto, 'body', true), this.userController.createToken);
  }
}

export default UserRoute;
