import ConfigController from "@controllers/config.controller";
import { Router } from "express";
import validationMiddleware from "@middlewares/validation.middleware";
import { CreateConfigDto, UpdateConfigDto } from "@dtos/config.dto";

class ConfigRoute {
  public path = '/api/v1/admin/config';
  public router = Router();
  private configController = new ConfigController();

  constructor(){
    this.initializeRoutes();
  }
  private initializeRoutes() {
    this.router.post(`${this.path}`, validationMiddleware(CreateConfigDto, 'body',true), this.configController.createConfig);
    this.router.put(`${this.path}`, validationMiddleware(UpdateConfigDto, 'body',true), this.configController.updateConfig);
  }
}

export default ConfigRoute;

