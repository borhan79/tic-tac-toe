import socketIo from "socket.io";
import { User } from "@interfaces/user.interface";


export interface SocketWithUser extends socketIo.Socket{
  user: User;
}
