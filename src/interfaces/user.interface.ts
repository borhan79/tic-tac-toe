export interface User {
  _id?: string;
  NickName?: string;
  FullName?: string;
  Credit?: number;
  Avatar?: string;
}

export interface DataStoredInToken {
  _id: string;
  NickName: string;
}
