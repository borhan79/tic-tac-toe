export interface Board {
  _id: string,
  BoardId: string,
  Owner: string,
  Opponent: string;
  Status: number;
  Result: string;
  Credit: number;
}
