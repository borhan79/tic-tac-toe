export interface Config {
  _id: string,
  configId: number,
  joinTimeout: number,
  moveTimeout: number,
  validCredits: number[],
  bannedWords: string[],
}
