import _ from "underscore";

class XoChecker {

  private static checkRows(row: Array<number | string>): string | number {
    if (new Set(row).size === 1)
      return row[0];
    return -1;
  }

  private static checkDiagonals(xoArray: Array<Array <string | number>>): string | number {
    let set1: Set<string | number> = new Set()
    let set2: Set<string | number> = new Set()

    for (let i=0;i< xoArray.length;i++){
      set1.add(xoArray[i][i])
      set2.add(xoArray[i][xoArray.length - i - 1])
    }
    if (set1.size === 1 )
      return xoArray[0][0]
    if (set2.size === 1 )
      return xoArray[0][xoArray.length - 1]
    return -1
  }
  private static arrayToMatrix(xoArray: Array<number | string>, width: number): Array<Array <string | number>>{
    return xoArray.reduce(function(rows, key, index) {
      return (index % width == 0 ? rows.push([key])
        : rows[rows.length - 1].push(key)) && rows;
    }, []);
  }

  public static checkWinner(xoArray: Array<number | string>) {
    let matrix = this.arrayToMatrix(xoArray, 3)
    let result: string | number = -1;

    for (let item of matrix) {
      result = this.checkRows(item);
      if (result !== -1)
        return result;
    }
    // transpose array
    for (let item of _.zip(...matrix)) {
      result = this.checkRows(item);
      if (result !== -1)
        return result;
    }
    return this.checkDiagonals(matrix);
  }

  public static checkEqual(xoArray: Array<number | string>){
    return xoArray.filter(item => item == 0).length == 0;
  }

}

export default XoChecker;
