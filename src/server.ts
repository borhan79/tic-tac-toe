process.env['NODE_CONFIG_DIR'] = __dirname + '/configs';

import UserRoute from "@routes/user.route";
import ConfigRoute from "@routes/config.route";
import 'dotenv/config';
import App from '@/app';
import validateEnv from '@utils/validateEnv';

validateEnv();

const app = new App([new UserRoute(), new ConfigRoute()]);

app.listen();
