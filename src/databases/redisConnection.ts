import config from 'config';
import { DbConfig } from '@interfaces/db.interface';

const { host, port }: DbConfig = config.get('redisConfig');

export const redisConnection = {
  host: host,
  port: port
};
