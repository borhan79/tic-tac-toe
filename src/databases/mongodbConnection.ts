import config from 'config';
import { DbConfig } from '@interfaces/db.interface';

const { host, port, database }: DbConfig = config.get('mongodbConfig');

export const mongodbConnection = {
  url: `mongodb://${host}:${port}/${database}`,
  options: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  },
};
