process.env['NODE_CONFIG_DIR'] = __dirname + '/configs';

import compression from 'compression';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';
import helmet from 'helmet';
import hpp from 'hpp';
import morgan from 'morgan';
import { connect, set } from 'mongoose';
import { mongodbConnection } from '@databases/mongodbConnection';
import { redisConnection } from '@databases/redisConnection';
import Routes from '@interfaces/route.interface';
import errorMiddleware from '@middlewares/error.middleware';
import { logger, stream } from '@utils/logger';
import {Server, createServer} from 'http';
import socketIo from 'socket.io';
import asyncRedis from 'async-redis';
import SocketController from "@controllers/socket.contoller";

class App {
  public app: express.Application;
  public server: Server;
  public io: socketIo.Server;
  public port: string | number;
  public env: string;
  public socketController;
  public static redisClient;

  constructor(routes: Routes[]) {
    this.app = express();
    this.server = createServer(this.app)
    this.port = process.env.PORT || 3000;
    this.env = process.env.NODE_ENV || 'development';

    this.connectToMongodb();
    this.connectToRedis();
    this.initializeMiddlewares();
    this.initializeRoutes(routes);
    this.initializeErrorHandling();
    this.initializeSocket();
  }

  private initializeSocket(): void {
    this.io = socketIo(this.server);
    this.socketController = new SocketController(this.io)

  }

  public listen(): void {
    this.server.listen(this.port, () => {
      logger.info(`=================================`);
      logger.info(`======= ENV: ${this.env} =======`);
      logger.info(`🚀 App listening on the port ${this.port}`);
      logger.info(`=================================`);
    });
  }

  private connectToRedis(){
    App.redisClient = asyncRedis.createClient(redisConnection)
  }

  public getServer(): express.Application {
    return this.app;
  }

  private connectToMongodb(): void {
    if (this.env !== 'production') {
      set('debug', true);
    }
    connect(mongodbConnection.url, mongodbConnection.options)
      .then(() => {
        logger.info('connected to Mongodb successfully');
      })
      .catch(err => {
        logger.error(`Error while connecting to Mongodb: ${err}`);
      });
  }

  private initializeMiddlewares(): void {
    if (this.env === 'production') {
      this.app.use(morgan('combined', { stream }));
      this.app.use(cors());
    } else {
      this.app.use(morgan('dev', { stream }));
      this.app.use(cors());
    }

    this.app.use(hpp());
    this.app.use(helmet());
    this.app.use(compression());
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
    this.app.use(cookieParser());
  }

  private initializeRoutes(routes: Routes[]): void {
    routes.forEach(route => {
      this.app.use('/', route.router);
    });
  }


  private initializeErrorHandling(): void {
    this.app.use(errorMiddleware);
  }
}

export default App;
