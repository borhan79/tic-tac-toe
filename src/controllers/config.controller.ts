import ConfigService from "@services/config.service";
import { Request, Response, NextFunction } from "express";
import HttpResponse from "@utils/httpResponse";
import { UserDto } from "@dtos/user.dto";
import { User } from "@interfaces/user.interface";
import { CreateConfigDto, UpdateConfigDto } from "@dtos/config.dto";
import { Config } from "@interfaces/config.interface";

class ConfigController {
  private configService = new ConfigService();

  public createConfig = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const configData: CreateConfigDto = req.body;
      const createConfigData: Config = await this.configService.createConfig(configData);
      res.status(201).send(new HttpResponse(201, 'Games Config created successfully', createConfigData));
    } catch (error) {
      next(error);
    }
  }
  public updateConfig = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const configData: UpdateConfigDto = req.body;
      const updateConfigData: Config = await this.configService.updateConfig(configData, configData.status);
      res.status(201).send(new HttpResponse(201, 'Games Config updated successfully', {}));
    } catch (error) {
      next(error);
    }
  }
}

export default ConfigController;
