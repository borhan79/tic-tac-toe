import UserService from "@services/user.service";
import { Request, Response, NextFunction } from "express";
import HttpResponse from "@utils/httpResponse";
import { DeleteUserDto, UserDto } from "@dtos/user.dto";
import { User } from "@interfaces/user.interface";

class UserController {
  private userService = new UserService()

  public createUser = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const userData: UserDto = req.body;
      const createUserData: User = await this.userService.createUser(userData);
      const token = await this.userService.createToken(createUserData);
      res.status(201).send(new HttpResponse(201, 'signup successfully', { token: token }));
    } catch (error) {
      next(error);
    }
  };

  public updateUser = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const userData: UserDto = req.body;
      const updateUserData: User = await this.userService.updateUser(userData);
      res.status(200).send(new HttpResponse(201, 'user updated successfully', {}));
    } catch (error) {
      next(error);
    }
  };

  public deleteUser = async (req: Request, res: Response, next: NextFunction) => {
    try {
      await this.userService.deleteUser(req.query.NickName as string);
      res.status(200).send(new HttpResponse(200, 'user deleted successfully', {}));
    } catch (error) {
      next(error);
    }
  };

  public createToken = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const userData: UserDto = req.body;
      const getUserData: User = await this.userService.getUser(userData.NickName);
      const token = await this.userService.createToken(getUserData);
      res.status(200).send(new HttpResponse(200, 'token sent successfully', { token: token }));
    } catch (error) {
      next(error);
    }
  };
}

export default UserController;
