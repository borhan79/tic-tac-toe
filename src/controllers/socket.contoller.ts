import socketIo from "socket.io";
import authMiddleware from "@middlewares/auth.middleware";
import { GameEvents } from "@enums/events.enum";
import { SocketWithUser } from "@interfaces/socket.interface";
import SocketService from "@services/socket.service";

class SocketController {
  private io: socketIo.Server;
  private socketService = new SocketService();

  constructor(socket: socketIo.Server) {
    this.io = socket;
    this.connection();
  }

  public connection() {
    this.io.use(authMiddleware)
      .on(GameEvents.CONNECT, (socket: SocketWithUser) => {

        this.io.to(socket.id).emit(GameEvents.SERVER_MESSAGE, `Welcome to  XO game ${socket.user.NickName}`);

        socket.on(GameEvents.JOIN_ROOM, async (roomId: string) => {
          await this.socketService.joinRoomEvent(this.io, socket, roomId);
        });


        socket.on(GameEvents.PLAYGROUND, async (index: number) => {
          await this.socketService.playGroundEvent(this.io, socket, index);

        });

        socket.on(GameEvents.END_GAME, async () => {
          await this.socketService.endGameEvent(this.io, socket);

        });

        socket.on(GameEvents.LOBBY, async () => {
          await this.socketService.lobbyEvent(this.io, socket);

        });

        socket.on(GameEvents.WAITING, async (credit: string) => {
          await this.socketService.waitingEvent(this.io, socket, parseInt(credit));

        });

        socket.on(GameEvents.SEND_MESSAGE, async (msg: string) => {
          await this.socketService.sendMessageEvent(this.io, socket, msg);
        });


        socket.on(GameEvents.DISCONNECT, () => {
          console.log("Client disconnected");
        });
      });
  }
}

export default SocketController;
