import { IsDefined, IsNumber, IsString } from "class-validator";

export class UserDto {
  @IsDefined()
  @IsString()
  public NickName: string;
  @IsString()
  public FullName?: string;
  @IsNumber()
  public Credit?: string;
  @IsString()
  public Avatar?: string;
}

export class DeleteUserDto {
  @IsString()
  public NickName: string;
}
