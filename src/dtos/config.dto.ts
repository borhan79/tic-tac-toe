import { IsDefined, IsNumber, IsString } from "class-validator";

export class CreateConfigDto {
  @IsNumber()
  public joinTimeout?: number;
  @IsNumber()
  public moveTimeout?: number;
  @IsNumber({},{each: true})
  public validCredits?: number[];
  @IsString({each: true})
  public bannedWords?: string[];
}

export class UpdateConfigDto {
  @IsDefined()
  @IsNumber()
  public status?: number;
  @IsNumber()
  public joinTimeout?: number;
  @IsNumber()
  public moveTimeout?: number;
  @IsNumber({},{each: true})
  public validCredits?: number[];
  @IsString({each: true})
  public bannedWords?: string[];
}
