import { model, Schema, Document } from "mongoose";
import { Config } from "@interfaces/config.interface";

const configSchema: Schema = new Schema({
  configId: {
    type: Number,
    default: 1
  },
  joinTimeout: {
    type: Number,
    default: 180
  },
  moveTimeout: {
    type: Number,
    default: 60
  },
  validCredits:
    {
      type: [Number],
      default: [50]
    },
  bannedWords:
    {
      type: [String]
    }
});

configSchema.methods.toJSON = function() {
  const obj = this.toObject();
  obj.id = obj._id;
  delete obj._id;
  delete obj.__v;
  return obj;
};

const configModel = model<Config & Document>("Config", configSchema);

export default configModel;
