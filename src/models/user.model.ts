import { model, Schema, Document } from 'mongoose';
import { User } from '@interfaces/user.interface';

const userSchema: Schema = new Schema({
  NickName: {
    type: String,
    required: true
  },
  FullName: {
    type: String,
    default: '',
  },
  Credit: {
    type: Number,
    default: 200,
  },
  Avatar: {
    type: String
  },
});

userSchema.methods.toJSON = function () {
  const obj = this.toObject();
  obj.id = obj._id;
  delete obj._id;
  delete obj.__v;
  return obj;
};

const userModel = model<User & Document>('User', userSchema);

export default userModel;
