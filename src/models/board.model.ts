import { model, Schema, Document } from "mongoose";
import { Board } from "@interfaces/board.interface";

const boardSchema: Schema = new Schema({
  BoardId: {
    type: String,
    required: true
  },
  Owner: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  Opponent: {
    type: Schema.Types.ObjectId,
    ref: "User"
  },
  Status: {
    type: Number,
    default: 0
  },
  Result: {
    type: String,
    default: ""
  },
  Credit: {
    type: Number,
    default: 0
  }
});

boardSchema.methods.toJSON = function() {
  const obj = this.toObject();
  obj.id = obj._id;
  delete obj._id;
  delete obj.__v;
  return obj;
};

const boardModel = model<Board & Document>("Board", boardSchema);

export default boardModel;
